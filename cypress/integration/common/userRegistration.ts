/* eslint-disable new-cap */
import {Given} from 'cypress-cucumber-preprocessor/steps';
import {generateUserData} from '../../support/userFactory';
import UserData = Cypress.UserData;

Given(`I register a new user with request`, () => {
  const userData:UserData = generateUserData();
  cy.wrap(userData).as('userData');
  cy.registerUser(userData);
});
