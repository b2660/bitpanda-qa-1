/* eslint-disable new-cap */

import {Then, When} from 'cypress-cucumber-preprocessor/steps';
import UserData = Cypress.UserData;

Then('I am logged in', () => {
  cy.visit('/');
  cy.get<UserData>('@userData').then((userData: UserData) => {
    cy.get('div.header_user_info')
        .find(`span:contains("${userData.name} ${userData.lastName}")`);
  });
});

When('I log out by clearing cookie', () => {
  cy.clearCookie('PrestaShop-a30a9934ef476d11b6cc3c983616e364');
});

Then('I log in again', () => {
  cy.visit({
    url: '/index.php',
    qs:
      {
        controller: 'authentication',
        back: 'my-account',
      },
  });

  cy.get<UserData>('@userData').then((userData: UserData) => {
    cy.get('#login_form').as('loginForm');
    cy.get('@loginForm').find('#email').type(userData.email);
    cy.get('@loginForm').find('#passwd').type(userData.password);
    cy.get('#SubmitLogin').click();

    cy.get('div.header_user_info')
        .find(`span:contains("${userData.name} ${userData.lastName}")`);
  });
});
