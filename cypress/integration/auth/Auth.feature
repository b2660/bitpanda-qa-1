Feature: Auth

  I want to register and login.

  IRL it would probably make sense to make it with gherkins params,
  but for time being I implicitly pass userdata in code, implying that
  there is a new single user per scenario.

  Scenario: New user registered
    Given I register a new user with request
    Then I am logged in

  Scenario: Login with existing user
    Given I register a new user with request
    When I log out by clearing cookie
    And I log in again
    Then I am logged in