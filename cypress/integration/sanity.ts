import Cookie = Cypress.Cookie;
import {generateUserData} from '../support/userFactory';

describe('Sanity run', () => {
  it('Search for women summer dresses by following pics', () => {
    cy.get('a.sf-with-ul:contains("Women")').click();
    cy.get('a.img[title="Dresses"]').click();
    cy.get('a.img[title="Summer Dresses"]').click();
    cy.get('ul.product_list').should('be.visible');
  });

  it('Search for T-Shirt using dropdown', () => {
    cy.get('a.sf-with-ul:contains("Women")')
        .rightclick()
        .parent()
        .find('a[title="T-shirts"]')
        .should('be.visible')
        .click();
    cy.get('ul.product_list').should('be.visible');
  });

  it('Search for women tops by following side panel', () => {
    cy.get('a.sf-with-ul:contains("Women")').click();
    cy.get('#categories_block_left')
        .find('a:contains("Tops")')
        .click();
    cy.get('ul.product_list').should('be.visible');
  });

  it('Add women top to the cart from the grid', () => {
    cy.request('/index.php?id_category=4&controller=category');

    cy.get('ul.product_list')
        .find('li.ajax_block_product')
        .first()
        .trigger('mouseover')
        .find('a.button[title="Add to cart"]')
        .should('be.visible')
        .click();

    cy.get('h2:contains("Product successfully added to your shopping cart")')
        .should('be.visible')
        .find('i.icon-ok')
        .should('be.visible');
  });

  it('Remove woman top from the cart drop down', () => {
    cy.request({
      url: '/index.php',
      qs: {
        id_product: '1',
        controller: 'cart',
        add: '1',
        token: 'e817bb0705dd58da8db074c69f729fd8',
      },
    });

    cy.reload();

    cy.get('div.shopping_cart')
        .trigger('mouseover', 'bottom')
        .find('span.remove_link')
        .should('be.visible')
        .click()
        .find('div.cart_block')
        .should('not.exist');
  });

  // We would definitely like to try registration with UI irl, but
  //  it is just another boring fields copy-pasting, so I omit it
  //  for this showcase.
  it('Register new user', () => {
    const userData = generateUserData();
    cy.registerUser(userData)
        .then((cookie: Cookie) => {
          cy.setCookie(cookie.name, cookie.value);
        });
    cy.visit('/');
    cy.get('div.header_user_info')
        .find(`span:contains("${userData.name} ${userData.lastName}")`);
  });

  it('Login', () => {
    cy.clearCookies();
    const userData = generateUserData();
    cy.registerUser(userData);
    cy.clearCookies();

    cy.visit({
      url: '/index.php',
      qs:
          {
            controller: 'authentication',
            back: 'my-account',
          },
    });

    cy.get('#login_form').as('loginForm');
    cy.get('@loginForm').find('#email').type(userData.email);
    cy.get('@loginForm').find('#passwd').type(userData.password);
    cy.get('#SubmitLogin').click();

    cy.url().should('eq',
        `${Cypress.config().baseUrl}/index.php?controller=my-account`);
    cy.get('h1:contains("My account")');
    cy.get('div.header_user_info')
        .find(`span:contains("${userData.name} ${userData.lastName}")`);
  });

  beforeEach(() => {
    cy.visit('/');
  });
});
