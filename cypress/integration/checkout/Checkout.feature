Feature: Checkout

  Scenario: Checking out cart with product
    Given I register a new user with request
    When I add item to cart
    And I checkout cart with item
    Then I see confirmation