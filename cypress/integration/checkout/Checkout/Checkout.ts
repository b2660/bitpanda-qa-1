/* eslint-disable new-cap */
import {And, Then, When} from 'cypress-cucumber-preprocessor/steps';

When('I add item to cart', () => {
  cy.visit('/index.php?id_category=4&controller=category');

  cy.get('ul.product_list')
      .find('.first-in-line > .product-container')
      .first()
      .trigger('mouseover')
      .find('a.button[title="Add to cart"]')
      .should('be.visible')
      .click();

  cy.get('h2:contains("Product successfully added to your shopping cart")')
      .should('be.visible')
      .find('i.icon-ok')
      .should('be.visible');
});

And('I checkout cart with item', () => {
  cy.visit('http://automationpractice.com/index.php?controller=order');
  cy.get('#order-detail-content').should('exist');
  cy.get('.cart_navigation > .button > span').click();
  cy.get('.cart_navigation > .button > span').click();
  cy.get('#cgv').check();
  cy.get('.cart_navigation > .button > span').click();
  cy.get('#HOOK_PAYMENT')
      .find('a.bankwire')
      .click();
  cy.get('button:contains("I confirm my order")').click();
});

Then('I see confirmation', () => {
  cy.get('p.cheque-indent')
      .find('strong:contains("Your order on My Store is complete.")')
      .should('be.visible');
});
