/// <reference types="cypress" />

const browserify = require('@cypress/browserify-preprocessor');
const resolve = require('resolve');
const cucumber = require('cypress-cucumber-preprocessor').default;

/**
 * @type {Cypress.PluginConfig}
 */
// eslint-disable-next-line no-unused-vars
module.exports = (on, config) => {
  const options = {
    ...browserify.defaultOptions,
    typescript: resolve.sync('typescript', {
      baseDir: config.projectRoot,
    }),
  };

  on('file:preprocessor', cucumber(options));
};
