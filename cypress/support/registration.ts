import {UserData} from './models/userData';
import Chainable = Cypress.Chainable;

/**
 * Registers provided user via register form
 * @param {UserData} userData User data used in register form
 * @return {Chainable} Yields session cookie that can be used to switch to
 * logged in session with registered user
 */
export function registrationRequest(userData: UserData): Chainable {
  const sessionCookieName = 'PrestaShop-a30a9934ef476d11b6cc3c983616e364';

  return cy.request({
    method: 'POST',
    url: '/index.php',
    form: true,
    qs: {
      controller: 'authentication',
    },
    body: {
      customer_firstname: userData.name,
      customer_lastname: userData.lastName,
      email: userData.email,
      passwd: userData.password,
      days: userData.birthDate.getDate(),
      months: userData.birthDate.getMonth(),
      years: userData.birthDate.getFullYear(),
      firstname: userData.name,
      lastname: userData.lastName,
      address1: userData.address,
      city: userData.city,
      id_state: userData.stateId,
      postcode: userData.zip,
      id_country: userData.countryId,
      phone_mobile: userData.phoneMobile,
      alias: 'My address',
      email_create: '1',
      is_new_customer: '1',
      back: 'my-account',
      submitAccount: '',
    },
  }).then( (response) => {
    assert.equal(response.body.indexOf('alert alert-danger'), -1);
    cy.log(userData.email);
    cy.log(userData.password);

    cy.getCookie(sessionCookieName)
        .should('exist')
        .then((cookie) => {
          cy.log(cookie.value);
        });
  });
}

