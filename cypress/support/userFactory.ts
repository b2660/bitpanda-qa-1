import {UserData} from './models/userData';

/**
 * Generate user data
 * @return {UserData}
 */
export function generateUserData(): UserData {
  const userData = new UserData();
  userData.email = `testEmail${new Date().getTime()}@test.com`;
  userData.name = 'test';
  userData.lastName = 'test';
  userData.password = 'test1';
  userData.address = 'Test 1, 1, 1';
  userData.city = 'TestCity';
  userData.stateId = '1';
  userData.zip = '00000';
  userData.phoneMobile = '00000';
  userData.birthDate = new Date(1994, 1, 1);
  userData.countryId = '21';
  return userData;
}
