import {UserData} from './models/userData';
import {registrationRequest} from './registration';

Cypress.Commands.add(
    'registerUser',
    (userData: UserData) => registrationRequest(userData));

