/// <reference types="cypress"/>

/**
 * Type definitions for custom commands
 * See {@link https://docs.cypress.io/guides/tooling/typescript-support#Types-for-custom-commands}
 */
// eslint-disable-next-line no-unused-vars
declare namespace Cypress {
    type UserData = import('./models/userData').UserData;
    interface Chainable {
        registerUser(userData: UserData): Chainable<Cookie | null>
    }
}
