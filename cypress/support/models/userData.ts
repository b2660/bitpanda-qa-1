/**
 * User POCO
 */
export class UserData {
  email : string;
  name : string;
  password : string;
  address : string;
  city : string;
  // US states in alphabetic order 1 to 50
  // Normally that would be a separate mapping e.g. enum or const map
  stateId : string;
  zip : string;
  country : string;
  phoneMobile : string;
  lastName: string;
  birthDate: Date;
  // Only available country for now is US with ID 21
  countryId: string;
}
