# Bitpanda QA 1

1st task from bitpanda QA assignment

## Case

Write an automated test for an e-commerce site, feel free to pick any: (e.g.
http://automationpractice.com/index.php):

● You could implement some of the following tests:

○ Login, searching for products using 3 different criteria, adding products
to the cart, removing products from the cart, checkout process, if
possible, implement a login and sign-up test;

○ Report any bugs you find by writing a bug ticket

● Please prepare a document with the test flows and test cases. The
documents have to be clear both to the developer and to someone who is not
familiar with the technology;

● Run tests in a continuous integration tool and optionally in the cloud;

● Please provide us steps on how to run the written test.

## Solution

I haven't been doing a lot of E2E lately, so as long as cypress was mentioned in tech stack - I took a chance to take a look at it while doing the assignment instead of recalling Selenium. 

- Tests are written in cypress and typescript
- Some tests are redundant because of 2 execution flows - with and without cucumber. By saying `documents have to be clear both to the developer and to someone who is not familiar with the technology` I believe you were hinting to it - so I just made few tests running of gherkin as an example.
- Tests can be ran from gitlab CI in this project - `CI/CD` on the left. ( https://docs.gitlab.com/ee/ci/quick_start/#view-the-status-of-your-pipeline-and-jobs )
- You can get report for test run from CI artifacts - open job log -> on the right panel `Download` ( https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#download-job-artifacts )
- I am not sure though what did you mean by cloud - hopefully Gitlab shared runner is something close to expectations. Otherwise I could spin up
extra gitlab runner on GCP free VMs - but I guess the result would be the same.
- You can check how to run tests from CLI from CI definitions in [.gitlab-ci.yml](./.gitlab-ci.yml) or you can run them with Cypress gui - from job 
description I trust you know how.

## Disclaimer

- These are not complete E2E. These tests are just some sanity runs to basically demonstrate that I can code and setup some CI stuff. These tests
are not to demonstrate the test coverage itself - irl number of testcases would be significantly higher ofc.
- Although Cypress recommends to run tests hand in hand with local develpment - I spared myself from creating SPA app and just pointed it to proposed http://automationpractice.com/index.php
- For CI - video collection for FF does not work. It seems to be actual issue with Cypress itself https://github.com/cypress-io/cypress/issues/18415 . Probably can be solved by duct tape with enough dedication though.
